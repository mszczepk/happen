package mszczepk.happen.support;

import java.time.Duration;

import static java.util.Arrays.stream;

public final class Validation {

   public static void requireNotNull(Object argument, String message) {
      if (argument == null) {
         throw new IllegalArgumentException(message);
      }
   }

   public static void requireInRangeExclusive(int min, int argument, int max, String message) {
      if (argument <= min || max <= argument) {
         throw new IllegalArgumentException(message);
      }
   }

   public static void requirePositive(Duration argument, String message) {
      if (argument == null || argument.isZero() || argument.isNegative()) {
         throw new IllegalArgumentException(message);
      }
   }

   public static void requireNotBlank(String argument, String message) {
      if (argument == null || argument.trim().isEmpty()) {
         throw new IllegalArgumentException(message);
      }
   }

   public static void requireNoNullElement(Object[] argument, String message) {
      if (argument == null || stream(argument).anyMatch(element -> element == null)) {
         throw new IllegalArgumentException(message);
      }
   }

   private Validation() {}
}
