package mszczepk.happen;

import mszczepk.happen.event.controller.EventEndpoint;
import mszczepk.happen.event.controller.EventCleanupJob;
import mszczepk.happen.event.domain.EventService;
import mszczepk.happen.infrastructure.JobExecutor;
import mszczepk.happen.infrastructure.WebServer;
import org.pmw.tinylog.Logger;
import java.time.Duration;

import static java.lang.Integer.parseInt;
import static java.time.Duration.ofMinutes;

final class Happen {

   public static void main(String[] args) throws Exception {
      Logger.info("Launching application...");
      // configuration
      String contextPath = "/happen";
      int port = 8080;
      Duration eventClenupTime = ofMinutes(1);
      Duration eventLifeTime = ofMinutes(parseArgs(args));
      Logger.info("Event life time set to {}", eventLifeTime);
      EventService eventService = new EventService(eventLifeTime);

      // server
      Class[] resources = new Class[] {EventEndpoint.class};
      Object[] beans = new Object[] {eventService};
      new WebServer(contextPath, port, resources, beans).service();

      // scheduler
      Runnable job = new EventCleanupJob(eventService);
      new JobExecutor().schedule(job, eventClenupTime);
      Logger.info("Application is running..." );
   }

   private static int parseArgs(String[] args) {
      try {
         return parseInt(args[0]);
      } catch (Exception e) {
         return 60;
      }
   }

   private Happen() {}
}
