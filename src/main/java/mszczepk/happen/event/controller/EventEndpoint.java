package mszczepk.happen.event.controller;

import mszczepk.happen.event.controller.dto.UserEventDto;
import mszczepk.happen.event.domain.EventService;
import mszczepk.happen.event.controller.dto.UserCountByEventDto;
import org.pmw.tinylog.Logger;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.time.Duration;
import java.util.List;
import java.util.Map;

import static java.time.Duration.ofMinutes;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;
import static mszczepk.happen.support.Validation.requireNotNull;

@Singleton
@Path("/")
public class EventEndpoint {

   private final EventService eventService;

   @Inject
   public EventEndpoint(EventService eventService) {
      requireNotNull(eventService, "event service must not be null!");
      this.eventService = eventService;
   }

   @POST
   @Path("register/event")
   @Produces(TEXT_PLAIN)
   public Boolean register(UserEventDto userEventDto) {
      String eventId = userEventDto.eventId;
      String userId = userEventDto.userId;
      Logger.info("Registering event! event id: {}, user id: {}", eventId, userId);
      return eventService.register(eventId, userId);
   }

   @GET
   @Path("views/{eventId}")
   @Produces(APPLICATION_JSON)
   public UserCountByEventDto get(@PathParam("eventId") String eventId,
                                  @QueryParam("n") Integer minutes) {
      Duration time = ofMinutes(minutes);
      Integer count = eventService.get(eventId, time);
      return map(eventId, count);
   }

   @POST
   @Path("views/all")
   @Produces(APPLICATION_JSON)
   public List<UserCountByEventDto> getAll(@QueryParam("n") Integer minutes) {
      Duration time = ofMinutes(minutes);
      Map<String, Integer> userCountsByEvent = eventService.getAll(time);
      return map(userCountsByEvent);
   }

   private List<UserCountByEventDto> map(Map<String, Integer> input) {
      return input.entrySet()
         .stream()
         .map(entry -> map(entry.getKey(), entry.getValue()))
         .collect(toList());
   }

   private UserCountByEventDto map(String eventId, Integer count) {
      UserCountByEventDto dto = new UserCountByEventDto();
      dto.eventId = eventId;
      dto.views = count;
      return dto;
   }
}
