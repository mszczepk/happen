package mszczepk.happen.event.controller;

import mszczepk.happen.event.domain.EventService;
import org.pmw.tinylog.Logger;

import static mszczepk.happen.support.Validation.requireNotNull;

public final class EventCleanupJob implements Runnable {

   private final EventService eventService;

   public EventCleanupJob(EventService eventService) {
      requireNotNull(eventService, "event service must not be null!");
      this.eventService = eventService;
   }

   @Override
   public void run() {
      Logger.info("Clearing expired events!");
      eventService.clear();
   }
}
