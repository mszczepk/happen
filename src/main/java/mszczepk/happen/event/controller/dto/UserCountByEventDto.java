package mszczepk.happen.event.controller.dto;

public class UserCountByEventDto {
   public String eventId;
   public Integer views;
}
