package mszczepk.happen.event.domain;

import org.pmw.tinylog.Logger;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collector;

import static java.lang.Integer.MAX_VALUE;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toSet;
import static mszczepk.happen.support.Validation.requireNotNull;

final class InMemoryEventRepository implements EventRepository {

   private final AtomicLong sequence;
   private final ConcurrentHashMap<Long, Event> database;

   InMemoryEventRepository() {
      this.sequence = new AtomicLong();
      this.database = new ConcurrentHashMap<>();
   }

   @Override
   public void insert(Event event) {
      requireNotNull(event, "event must not be null!");
      Long id = sequence.incrementAndGet();
      database.put(id, event);
   }

   @Override
   public synchronized void deleteExpired() {
      database.values().removeIf(event -> event.isExpired());
   }

   @Override
   public Integer countUsersForEventInPeriod(String eventId, TimePeriod period) {
      long count = database.values()
         .stream()
         .filter(event -> event.hasEventId(eventId))
         .filter(event -> event.isNotExpired())
         .filter(event -> event.occuredIn(period))
         .map(event -> event.toUserId())
         .distinct()
         .count();
      if (count > MAX_VALUE) {
         Logger.warn("Users count for event overflowed max integer value!");
      }
      return (int) count;
   }

   @Override
   public Map<String, Integer> countUsersByEventInPeriod(TimePeriod period) {
      return database.values()
         .stream()
         .filter(event -> event.isNotExpired())
         .filter(event -> event.occuredIn(period))
         .collect(
            groupingBy(event -> event.toEventId(),
            countingDistinctUsers())
         );
   }

   private static Collector<Event, ?, Integer> countingDistinctUsers() {
      return mapping(
         event -> event.toUserId(),
         collectingAndThen(
            toSet(),
            counting()
         )
      );
   }

   private static Function<Set<String>, Integer> counting() {
      return set -> set.size();
   }
}
