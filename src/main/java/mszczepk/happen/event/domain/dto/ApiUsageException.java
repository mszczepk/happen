package mszczepk.happen.event.domain.dto;

public class ApiUsageException extends RuntimeException {

   public ApiUsageException(String message) {
      super(message);
   }
}
