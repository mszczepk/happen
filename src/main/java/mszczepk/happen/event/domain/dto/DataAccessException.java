package mszczepk.happen.event.domain.dto;

public class DataAccessException extends RuntimeException {

   public DataAccessException(Throwable cause, String message) {
      super(message, cause);
   }
}
