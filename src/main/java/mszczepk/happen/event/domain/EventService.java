package mszczepk.happen.event.domain;

import mszczepk.happen.event.domain.dto.ApiUsageException;
import mszczepk.happen.event.domain.dto.DataAccessException;
import java.time.Duration;
import java.util.Map;

import static java.lang.String.format;
import static mszczepk.happen.event.domain.TimePeriod.past;
import static mszczepk.happen.support.Validation.requireNotNull;
import static mszczepk.happen.support.Validation.requirePositive;

public final class EventService {

   public EventService(Duration eventLifeTime) {
      this(eventLifeTime, new InMemoryEventRepository());
   }

   public boolean register(String eventId, String userId) {
      try {
         Event event = new Event(eventId, userId, eventLifetime);
         eventRepository.insert(event);
         return true;
      } catch (IllegalArgumentException | DataAccessException e) {
         return false;
      }
   }

   public void clear() {
      eventRepository.deleteExpired();
   }

   public int get(String eventId, Duration time) {
      requireNotGreaterThanEventLifeTime(time);
      TimePeriod period = past(time);
      return eventRepository.countUsersForEventInPeriod(eventId, period);
   }

   public Map<String, Integer> getAll(Duration time) {
      requireNotGreaterThanEventLifeTime(time);
      TimePeriod period = past(time);
      return eventRepository.countUsersByEventInPeriod(period);
   }

   private EventService(Duration eventLifetime, EventRepository eventRepository) {
      requirePositive(eventLifetime, "event life time must be > 0!");
      requireNotNull(eventRepository, "event repository must not be null!");
      this.eventLifetime = eventLifetime;
      this.eventRepository = eventRepository;
   }

   private final Duration eventLifetime;
   private final EventRepository eventRepository;

   private void requireNotGreaterThanEventLifeTime(Duration time) {
      requirePositive(time, "queried period must be > 0!");
      if (time.compareTo(eventLifetime) > 0) {
         String pattern = "queried period (%s) must be <= event life time (%s)!";
         String message = format(pattern, time, eventLifetime);
         throw new ApiUsageException(message);
      }
   }
}
