package mszczepk.happen.event.domain;

import java.time.Duration;
import java.time.Instant;

import static java.time.Instant.now;
import static mszczepk.happen.support.Validation.requireNotBlank;
import static mszczepk.happen.support.Validation.requirePositive;

final class Event {

   private final String eventId;
   private final String userId;
   private final Instant occurence;
   private final Instant expiration;

   Event(String eventId, String userId, Duration lifetime) {
      requireNotBlank(eventId, "event id must not be blank!");
      requireNotBlank(userId, "user id must not be blank!");
      requirePositive(lifetime, "event life time must be > 0!");
      this.eventId = eventId;
      this.userId = userId;
      this.occurence = now();
      this.expiration = occurence.plus(lifetime);
   }

   boolean isExpired() {
      return expiration.isBefore(now());
   }

   boolean isNotExpired() {
      return !isExpired();
   }

   boolean hasEventId(String id) {
      return eventId.equals(id);
   }

   boolean occuredIn(TimePeriod period) {
      return period.containsInclusive(occurence);
   }

   String toEventId() {
      return eventId;
   }

   String toUserId() {
      return userId;
   }
}
