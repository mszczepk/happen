package mszczepk.happen.event.domain;

import java.util.Map;

interface EventRepository {

   void insert(Event event);

   void deleteExpired();

   Map<String, Integer> countUsersByEventInPeriod(TimePeriod period);

   Integer countUsersForEventInPeriod(String eventId, TimePeriod period);
}
