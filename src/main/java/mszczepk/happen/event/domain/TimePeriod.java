package mszczepk.happen.event.domain;

import java.time.Duration;
import java.time.Instant;

import static java.time.Instant.now;
import static mszczepk.happen.support.Validation.requireNotNull;
import static mszczepk.happen.support.Validation.requirePositive;

final class TimePeriod {

   private final Instant start;
   private final Instant end;

   static TimePeriod past(Duration time) {
      requirePositive(time, "period time must be > 0!");
      Instant end = now();
      Instant start = end.minus(time);
      return new TimePeriod(start, end);
   }

   boolean containsInclusive(Instant timestamp) {
      requireNotNull(timestamp, "timestamp must not be null!");
      return isEqualOrBefore(start, timestamp) && isEqualOrBefore(timestamp, end);
   }

   private TimePeriod(Instant start, Instant end) {
      this.start = start;
      this.end = end;
   }

   private static boolean isEqualOrBefore(Instant subject, Instant toCompare) {
      return subject.compareTo(toCompare) <= 0;
   }
}
