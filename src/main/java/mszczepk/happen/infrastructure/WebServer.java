package mszczepk.happen.infrastructure;

import com.sun.net.httpserver.HttpServer;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.pmw.tinylog.Logger;
import java.net.URI;

import static javax.ws.rs.core.UriBuilder.fromUri;
import static mszczepk.happen.support.Validation.requireInRangeExclusive;
import static mszczepk.happen.support.Validation.requireNoNullElement;
import static mszczepk.happen.support.Validation.requireNotBlank;
import static org.glassfish.jersey.jdkhttp.JdkHttpServerFactory.createHttpServer;

public final class WebServer {

   private final ResourceConfig config;
   private final URI contextUri;
   private HttpServer server;

   public WebServer(String contextPath, int port, Class<?>[] resources, Object[] beans) {
      requireStartsWithSlash(contextPath);
      requireInRangeExclusive(0, port, 65536, "server port must be number in range (0, 65536)!");
      requireNoNullElement(resources, "JAX-RS resources array must not be null or contain null element!");
      requireNoNullElement(beans, "injectable beans array must not be null or contain null element!");

      this.config = new ResourceConfig();
      registerResources(resources);
      registerBeans(beans);
      this.contextUri = fromUri("http://localhost" + contextPath).port(port).build();
      this.server = null;
   }

   private void registerResources(Class<?>[] resources) {
      for (Class<?> resource : resources) {
         config.register(resource);
      }
   }

   private void registerBeans(Object[] beans) {
      config.registerInstances(new AbstractBinder() {
         @Override
         protected void configure() {
            for (Object bean : beans) {
               bind(bean).to(bean.getClass());
            }
         }
      });
   }

   public void service() {
      if (server == null) {
         this.server = createHttpServer(contextUri, config);
         Logger.info("Server started! uri: {}", contextUri);
      }
   }

   private void requireStartsWithSlash(String contextPath) {
      requireNotBlank(contextPath, "server context path must not be blank!");
      if (!contextPath.startsWith("/")) {
         throw new IllegalArgumentException("server context path must start with '/'!");
      }
   }
}
