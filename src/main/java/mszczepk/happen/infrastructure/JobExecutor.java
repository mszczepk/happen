package mszczepk.happen.infrastructure;

import org.pmw.tinylog.Logger;
import java.time.Duration;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.Executors.newSingleThreadScheduledExecutor;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static mszczepk.happen.support.Validation.requireNotNull;
import static mszczepk.happen.support.Validation.requirePositive;

public final class JobExecutor {

   private final ScheduledExecutorService executor;

   public JobExecutor() {
      this.executor = newSingleThreadScheduledExecutor();
   }

   public void schedule(Runnable job, Duration period) {
      requireNotNull(job, "job must not be null!");
      requirePositive(period, "job period must be >= 0!");
      long millis = period.toMillis();
      executor.scheduleAtFixedRate(job, millis, millis, MILLISECONDS);
      Logger.info("Job scheduled! period: {}", period.toString());
   }
}
