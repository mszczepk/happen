package mszczepk.happen

import groovyx.net.http.RESTClient
import spock.lang.Specification
import java.util.concurrent.Callable
import java.util.concurrent.Future

import static java.lang.System.currentTimeMillis
import static java.util.concurrent.Executors.newSingleThreadExecutor

class SystemStressIT extends Specification {

   // configuration
   static final TEST_TIME_MILLIS = 3 * 60 * 1000L
   static final USER_SLEEP_TIME_MILLIS = 0
   static final DISTINCT_EVENTS_COUNT = 15
   static final DISTINCT_USERS_COUNT = 30
   static final MAX_RESPONSE_TIME_MILLIS = 100L
   static final REGISTER_RATIO = 3
   static final GET_RATIO = 7
   static final N = 1 // see 'M' value in pre-integration-test phase in pom.xml

   def 'system should respond in reasonable time when handling multiple users'() {
      given:
      def users = (1..DISTINCT_USERS_COUNT).collect { new TestUser() }

      when:
      users.each { it.start() }
      sleep TEST_TIME_MILLIS
      users.each { it.stop() }
      and:
      def results = users.collect { it.result() }

      then:
      def getTimes = results.collectMany { it.get }
      assertAverageIsAcceptable(getTimes, 'get time')
      and:
      def registerTimes = results.collectMany { it.register }
      assertAverageIsAcceptable(registerTimes, 'register time')
      and:
      def allTimes = results.collectMany { it.all() }
      assertAverageIsAcceptable(allTimes, 'request time')
   }

   class TestUser {
      def events = (1..DISTINCT_EVENTS_COUNT).collect { "e${it}" as String }
      def userId = uuid()
      def client = new RESTClient('http://localhost:8080/happen/')
      def getResponseTimes = []
      def registerResponseTimes = []
      def random = new Random()
      def run = true

      Future<Times> result

      def start() {
         result = newSingleThreadExecutor().submit task()
      }

      def result() {
         result.get()
      }

      def task() {
         [ call: {
            while (run) {
               def action = random.nextInt(REGISTER_RATIO + GET_RATIO + 1)
               if (action <= REGISTER_RATIO) {
                  register()
               } else {
                  get()
               }
               sleep USER_SLEEP_TIME_MILLIS
            }
            new Times(getResponseTimes, registerResponseTimes)
         }] as Callable<Times>
      }

      def stop() {
         run = false
      }

      def register() {
         def index = random.nextInt(events.size())
         def eventId = events[index]
         def responseTime = currentTimeMillis()

         def response = client.post(
            path: 'register/event',
            body: [eventId: eventId, userId: userId],
            requestContentType: 'application/json'
         )
         assert response.status == 200

         responseTime = currentTimeMillis() - responseTime
         registerResponseTimes << responseTime
      }

      def get() {
         def index = random.nextInt(events.size())
         def eventId = events[index]
         def responseTime = currentTimeMillis()

         def response = client.get(
            path: "views/${eventId}",
            query: [n: N]
         )
         assert response.status == 200

         responseTime = currentTimeMillis() - responseTime
         getResponseTimes << responseTime
      }
   }

   class Times {
      List<Long> get
      List<Long> register

      Times(List<Long> get, List<Long> register) {
         this.get = get
         this.register = register
      }

      List<Long> all() {
         [*get, *register]
      }
   }

   static uuid() {
      UUID.randomUUID().toString()
   }

   static void assertAverageIsAcceptable(List<Long> list, String label) {
      def avg = list.sum() / list.size()
      println "${label} ${avg}"
      assert avg < MAX_RESPONSE_TIME_MILLIS
   }
}
