package mszczepk.happen

import groovyx.net.http.RESTClient
import spock.lang.Specification

class SystemIT extends Specification {

   static OK = 200
   static TEXT_PLAIN = 'text/plain'
   static APPLICATION_JSON = 'application/json'

   def 'register event and gets response should succeed'() {
      given:
      def client = new RESTClient('http://localhost:8080/happen/')
      and:
      def N = 1 // see 'M' value in pre-integration-test phase in pom.xml
      def eventId = 'e1'
      def response

      when:
      response = client.post(
         path: 'views/all',
         query: [n: N]
      )
      then:
      response.status == OK
      response.contentType == APPLICATION_JSON
      response.data == []

      when:
      response = client.post(
         path: 'register/event',
         body: [eventId: eventId, userId: uuid()],
         requestContentType: APPLICATION_JSON
      )
      then:
      response.status == OK
      response.contentType == TEXT_PLAIN
      response.data.text == 'true'

      when:
      response = client.post(
         path: 'views/all',
         query: [n: N]
      )
      then:
      response.status == OK
      response.contentType == APPLICATION_JSON
      response.data == [[eventId: eventId, views: 1]]

      when:
      response = client.get(
         path: "views/${eventId}",
         query: [n: N]
      )
      then:
      response.status == OK
      response.contentType == APPLICATION_JSON
      response.data == [eventId: eventId, views: 1]

      when:
      response = client.post(
         path: 'register/event',
         body: [eventId: eventId, userId: uuid()],
         requestContentType: APPLICATION_JSON
      )
      then:
      response.status == OK
      response.contentType == TEXT_PLAIN
      response.data.text == 'true'

      when:
      response = client.get(
         path: "views/${eventId}",
         query: [n: N]
      )
      then:
      response.status == OK
      response.contentType == APPLICATION_JSON
      response.data == [eventId: eventId, views: 2]

      when:
      response = client.get(
         path: 'views/unregistered-event',
         query: [n: N]
      )
      then:
      response.status == OK
      response.contentType == APPLICATION_JSON
      response.data == [eventId: 'unregistered-event', views: 0]

      when:
      response = client.post(
         path: 'register/event',
         body: [eventId: '', userId: uuid()],
         requestContentType: APPLICATION_JSON
      )
      then:
      response.status == OK
      response.contentType == TEXT_PLAIN
      response.data.text == 'false'
   }

   static uuid() {
      UUID.randomUUID().toString()
   }
}
