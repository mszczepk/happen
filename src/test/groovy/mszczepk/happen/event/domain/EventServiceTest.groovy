package mszczepk.happen.event.domain

import mszczepk.happen.event.domain.dto.ApiUsageException
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

import static java.time.Duration.ofSeconds

class EventServiceTest extends Specification {

   final static EVENT_A = 'e1'
   final static EVENT_B = 'e2'
   final static EVENT_C = 'e3'

   final static EVENT_LIFE_TIME = ofSeconds(2)
   final static QUERIED_TIME = ofSeconds(1)

   @Subject
   final eventService = new EventService(EVENT_LIFE_TIME)

   final user1 = new TestUser(eventService, 'u1')
   final user2 = new TestUser(eventService, 'u2')
   final user3 = new TestUser(eventService, 'u3')

   def 'when multiple registers then gets should return users count'() {
      when:
      user1.registerEvent(EVENT_A)
      user1.registerEvent(EVENT_B)
      user1.registerEvent(EVENT_C)
      and:
      user2.registerEvent(EVENT_A)
      user2.registerEvent(EVENT_B)
      and:
      user3.registerEvent(EVENT_A)

      then:
      eventService.get(EVENT_A, QUERIED_TIME) == 3
      eventService.get(EVENT_B, QUERIED_TIME) == 2
      eventService.get(EVENT_C, QUERIED_TIME) == 1
      and:
      eventService.getAll(QUERIED_TIME) == [(EVENT_A): 3, (EVENT_B): 2, (EVENT_C): 1]
   }

   def 'when duplicate registers then gets should return users count without duplicates'() {
      when:
      user1.registerEvent(EVENT_A)
      user1.registerEvent(EVENT_A)
      user1.registerEvent(EVENT_A)

      then:
      eventService.get(EVENT_A, QUERIED_TIME) == 1
      and:
      eventService.getAll(QUERIED_TIME) == [(EVENT_A): 1]
   }

   def 'when no registers then gets should return no users count'() {
      expect:
      eventService.get(EVENT_A, QUERIED_TIME) == 0
      eventService.get(EVENT_B, QUERIED_TIME) == 0
      eventService.get(EVENT_C, QUERIED_TIME) == 0
      and:
      eventService.getAll(QUERIED_TIME) == [:]
   }

   def 'gets should return users count for only non-expired events'() {
      given:
      def eventExpireTime = EVENT_LIFE_TIME.getSeconds() * 1000 // millis

      when:
      user1.registerEvent(EVENT_A)
      and:
      sleep eventExpireTime
      and:
      user1.registerEvent(EVENT_B)

      then:
      eventService.get(EVENT_A, QUERIED_TIME) == 0
      and:
      eventService.get(EVENT_B, QUERIED_TIME) == 1
      and:
      eventService.getAll(QUERIED_TIME) == [(EVENT_B): 1]
   }

   def 'gets should fail given queried time greater than event life time'() {
      given:
      def queriedTime = ofSeconds(2)
      def eventLifeTime = ofSeconds(1)
      and:
      def eventFacade = new EventService(eventLifeTime)

      when:
      eventFacade.get(EVENT_A, queriedTime)
      then:
      thrown ApiUsageException

      when:
      eventFacade.getAll(queriedTime)
      then:
      thrown ApiUsageException
   }

   @Unroll
   def 'gets should succeed given #description'() {
      given:
      def eventFacade = new EventService(eventLifeTime)

      when:
      eventFacade.get(eventId, queriedTime)
      then:
      noExceptionThrown()

      when:
      eventFacade.getAll(queriedTime)
      then:
      noExceptionThrown()

      where:
      queriedTime  | eventLifeTime   | eventId | description
      ofSeconds(2) | ofSeconds(2)    | EVENT_A | 'queried time and event life time are equal'
      QUERIED_TIME | EVENT_LIFE_TIME | null    | 'event id is null'
      QUERIED_TIME | EVENT_LIFE_TIME | ' '     | 'event id is blank'
   }

   def 'register should return true if succeed'() {
      expect:
      eventService.register(EVENT_A, 'u1')
   }

   class TestUser {
      final userId
      final facade

      TestUser(EventService facade, String userId) {
         this.facade = facade
         this.userId = userId
      }

      def registerEvent(String eventId) {
         facade.register(eventId, userId)
      }
   }
}
