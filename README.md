Happen
======
Simple application for counting user events.

Requirements
------------
- JDK 8
- Git
- curl (or other HTTP client)

Build
-----
- Run unit tests:  
`./mvnw clean test`

- Build executable:  
`./mvnw clean package -DskipTests`

- Run executable:  
`java -jar target/happen-1.0.0.jar <M>`  
where:  
M - (optional) time, in minutes, after which events are be to removed
    from the system from the moment of their registration; default is 60

- Run integration tests:  
`./mvnw clean verify`  

  **Note:** Currently, integration tests last about 3 minutes.
            See configuration in _SystemStressIT.groovy_ for more details.

Usage
-----
- Register event 'e1' as user 'u1':
```
curl -i -X POST -H 'Content-Type: application/json' -d '{"eventId": "e1", "userId": "u1"}' http://localhost:8080/happen/register/event
```

- Get count of unique users that registered 'e1' event within last 2 minutes:
```
curl -i -X GET -H 'Accept: application/json' http://localhost:8080/happen/views/e1?n=2
```

- Get all count of unique users grouped by events within last 2 minutes:
```
curl -i -X POST -H 'Accept: application/json' http://localhost:8080/happen/views/all?n=2
```

Author
------
Mateusz Szczepkowski
